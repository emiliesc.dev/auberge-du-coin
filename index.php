<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

get_header();

if(have_posts()){
    while(have_posts()){
        the_post();
        the_title();
        the_post();
    }
}
get_footer();
rewind_posts();
