<?php
add_action('wp_enqueue_scripts', 'enqueue_aubergeducoin_style');
if (!function_exists('enqueue_aubergeducoin_style')) {
    function enqueue_aubergeducoin_style()
    {
        wp_enqueue_style('aubergeducoin',
            get_stylesheet_directory_uri() . '/style.css',
            array('generatepress'),
            wp_get_theme()->get('Version')
        );
    }
}
?>


